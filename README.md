# python-gns3-api

>  this library is under development

Retrieve informations about your GNS3 VM - May be used for CI/CD deployment
Git --> Jenkins --> GNS3 --> Apply Modif in GNS3 --> Tests in GNS3 --> (if ok) --> Apply Modif in Prod --> Tests in prod --> (if nok) Rollback / (if ok) Notification

Tools can be used for Apply modif and tests in GNS3 and prod ```Ansible | Puppet | Nornir | Salt | Chef | etc.```

Functions available :

```
GET :
    * [26-February-2019] - (API) Get GNS3 VM Version
    * [11-March-2019]    - (API) Get Available appliances

    * [26-February-2019] - (API) Get Project_ID by Project_Name
    * [26-February-2019] - (API) Get Project_Information by Project_Name
    * [26-February-2019] - (API) Get Project_Information by Project_ID
    * [26-February-2019] - (API) Get Nodes_Information by Project_ID
    * [26-February-2019] - (API) Get Nodes_Information by Project_Name
	* [26-February-2019] - (API) Get All_Nodes_ID by Project_Name.
    * [26-February-2019] - (API) Get All_Nodes_ID by Project_ID.
    * [05-March-2019]    - (API) Get Node_Name by Project_ID and Node_ID
    * [05-March-2019]    - (API) Get NodesName by project_ID
    * [05-March-2019]    - (API) Get NodesName by project_Name
	* [06-March-2019]    - (API) Get Nodes_ID by Project_Name and Node_Name

    * [26-February-2019] - (SSH) Get Node_Startup-Config by Project_ID and Node_ID 
    * [05-March-2019]    - (SSH) Get Config File, not the content of Cumulus /etc (ls /etc) by Project_ID and Node_ID.
    * [06-March-2019]    - (SSH) Get Config File, not the content of Cumulus /etc (ls /etc) by Project_Name and Node_Name.
  
POST :
    * [26-February-2019] - (API) Start All Nodes from a Project_ID
    * [26-February-2019] - (API) Start All Nodes from a Project_Name
    * [26-February-2019] - (API) Stop All Nodes from a Project_ID
    * [26-February-2019] - (API) Stop All Nodes from a Project_Name
```

