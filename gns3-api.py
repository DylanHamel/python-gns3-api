#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
"""
This module use GNS3 VM API.

Below a list of GET and POST functions that are available
GET :
    * Get GNS3 VM Version.
     
    * (API) Get Project_ID by Project_Name.
    * (API) Get Project_Information by Project_Name.
    * (API) Get Project_Information by Project_ID.
    * (API) Get Nodes_Information by Project_ID.
    * (API) Get Nodes_Information by Project_Name.
    * (API) Get Node_Name by Project_ID and Node_ID
    * (API) Get Nodes_Name by Project_ID
    * (API) Get Nodes_Name by Project_Name
    * (API) Get Nodes_ID by Project_Name and Node_Name
    * (API) Get Available appliances
    

    * (SSH) Get Node_Startup-Config (Cisco) by Project_ID and Node_ID.
    * (SSH) Get Config File, not the content of Cumulus /etc (ls /etc) by Project_ID and Node_ID.
    * (SSH) Get Config File, not the content of Cumulus /etc (ls /etc) by Project_Name and Node_Name.

POST :
    * (API) Start All Nodes from a Project_ID.
    * (API) Start All Nodes from a Project_Name.
    * (API) Stop All Nodes from a Project_ID.
    * (API) Stop All Nodes from a Project_Name.


"""

# Default value used for exit()
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

try:
    import paramiko
except ImportError as importError:
    print("Error import paramiko")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import requests
except ImportError as importError:
    print("Error import requests")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json
except ImportError as importError:
    print("Error import json")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import click
except ImportError as importError:
    print("Error import click")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import sphinx
except ImportError as importError:
    print("Error import sphinx")
    print(importError)
    exit(EXIT_FAILURE)


@click.command()
@click.option('--ip', default="localhost", help='GNS3 VM IPv4 address.')
@click.option('--port', default="3080", help='GNS3 VM tcp port.')
def main(ip, port):

    print(getGNS3CumulusNodeConfigByProjectaNameAndNodeName(
            ip, port, "Cumulus-Spine-and-Leaf.gns3", "Spine01"))
        

        #print(getGNS3CumulusNodeConfigByProjectIDAndNodeID(
        #    ip, port, "72d96ebf-9436-4b28-b4ea-40402870515d", "a36a7983-4a44-48eb-a87a-b43b717b1605"))
        
        #print(getGNS3NodeNameByProjectIDAndNodeID(ip, port, "72d96ebf-9436-4b28-b4ea-40402870515d", "5435c50a-8fb0-4fa1-8a46-1270feb4ee11"))
        #print(getGNS3NodesNameByProjectID(ip, port, "72d96ebf-9436-4b28-b4ea-40402870515d"))
        #print(getGNS3NodesNameByProjectName(ip, port, "Cumulus-Spine-and-Leaf.gns3"))

        #print(getGNS3ProjectByName(ip, port, "Cumulus-Spine-and-Leaf.gns3"))
        #print(getGNS3ProjectIDByName(ip, port, "Cumulus-Spine-and-Leaf.gns3"))
        #print(getGNS3NodesIDByProjectName(ip, port, "Cumulus-Spine-and-Leaf.gns3"))

        # stopGNS3NodesByProjectID(ip, port, "4a0686cf-597b-4cc2-b811-bfed024cc03d")
        #print(getGNS3NodeConfigByProjectIDAndNodeID(ip, port, "4a0686cf-597b-4cc2-b811-bfed024cc03d", "b2d9f8a3-0fdc-4008-8e06-e1ef19da3841"))
        
        #liste1 = getGNS3NodesIDByProjectID(ip, port, "4a0686cf-597b-4cc2-b811-bfed024cc03d")
        #liste2 = getGNS3NodesIDByProjectName(ip, port, "IGMP-Snooping.gns3")
        #print("LISTE =" , liste1)
        #print("LISTE =", liste2)

    print(getGNS3ApplianceAvailables(ip,port))
    

    exit(EXIT_SUCCESS)

# -----------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------
# According to Nodes configuration


def getGNS3CumulusNodeConfigByProjectaNameAndNodeName(ip, port, project_name, node_name):
    """
    This function will return a string that node configuration according to project_name and node_name given in parameter
    This function only works with Cumulus Network Nodes !!

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project Name.
        param3 (str): GNS3 Node Name.

    Returns:
        list: list of configuration files
    """

    project_id = getGNS3ProjectIDByName(ip, port, project_name)
    return getGNS3CumulusNodeConfigByProjectIDAndNodeID(ip, port, project_id, getGNS3NodeIDByProjectIDAndNodeName(ip, port, project_id, node_name))

def getGNS3CumulusNodeConfigByProjectIDAndNodeID(ip, port, project_id, node_id):
    """
    This function will return a string that node configuration according to project_id and node_id given in parameter
    This function only works with Cumulus Network Nodes !!

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project ID.
        param3 (str): GNS3 Node ID.

    Returns:
        list: list of configuration files
    """
    
    sshClient = paramiko.SSHClient()
    sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    sshClient.connect(hostname="10.255.255.132",
                      username="gns3", password="gns3")

    stdin, stdout, stderr = sshClient.exec_command(
        "ls " + "/opt/gns3/projects/"+project_id+"/project-files/qemu/"+node_id+"/ | grep qcow2")
    output = stdout.readlines()
    if output.__len__() is not 0:
        stdin, stdout, stderr = sshClient.exec_command(
            "sudo modprobe nbd")
        stdout.readlines()
        stdin, stdout, stderr = sshClient.exec_command(
            "sudo qemu-nbd -c /dev/nbd0 /opt/gns3/projects/"+project_id+"/project-files/qemu/"+node_id+"/hda_disk.qcow2")
        stdout.readlines()
        stdin, stdout, stderr = sshClient.exec_command(
            "sudo mount /dev/nbd0p4 /mnt/cumulus01/")
        stdout.readlines()
        stdin, stdout, stderr = sshClient.exec_command(
            "sudo ls /mnt/cumulus01/etc/")
        output = stdout.readlines()

        cumulusNetworkConfigurationFilesInETC = list()
        for configFile in output:
            cumulusNetworkConfigurationFilesInETC.append(configFile[:-1])

        stdin, stdout, stderr = sshClient.exec_command(
            "sudo umount /mnt/cumulus01")
        stdout.readlines()
        stdin, stdout, stderr = sshClient.exec_command(
            "sudo qemu-nbd --disconnect /dev/nbd0")
        o = "".join(stdout.readlines())
        if "nbd0" not in o:
            raise Exception("Error during nbd disconnect")

        return cumulusNetworkConfigurationFilesInETC

def getGNS3CiscoNodeConfigByProjectIDAndNodeID(ip, port, project_id, node_id):
    """
    This function will return a string that node configuration according to project_id and node_id given in parameter
    This function only works with Cisco Nodes !!

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project ID.
        param3 (str): GNS3 Node ID.

    Returns:
        str: String Hostname
    """
    sshClient = paramiko.SSHClient()
    sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    sshClient.connect(hostname="10.255.255.132",
                      username="gns3", password="gns3")

    stdin, stdout, stderr = sshClient.exec_command(
        "ls " + "/opt/gns3/projects/"+project_id+"/project-files/dynamips/"+node_id + "/configs | grep startup-config")

    configFilename = stdout.readlines()
    path = "/opt/gns3/projects/"+project_id+"/project-files/dynamips/" + \
        node_id+"/configs/"+str(configFilename[0])

    stdin, stdout, stderr = sshClient.exec_command("cat " + path)

    nodeConfigurationFile = "".join(stdout.readlines())

    return nodeConfigurationFile

# -----------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------
# According to Project's Nodes

def getGNS3NodesNameByProjectName(ip, port, project_name):
    """
    This function will return a list that contains all node_name according to project_name given in parameter

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project Name.

    Returns:
        list: List that contains all nodes_name
    """

    return getGNS3NodesNameByProjectID(ip, port, getGNS3ProjectIDByName(ip, port, project_name))



def getGNS3NodesNameByProjectID(ip, port, project_id):
    """
    This function will return a list that contains all node_name according to project_id given in parameter

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project ID.

    Returns:
        list: List that contains all nodes_name
    """
    nodesID = list()
    nodesName = list()
    nodesID = getGNS3NodesIDByProjectID(ip,port,project_id)

    for nodeID in nodesID:
        nodesName.append(getGNS3NodeNameByProjectIDAndNodeID(
            ip, port, project_id, nodeID))

    return nodesName

def getGNS3NodeIDByProjectIDAndNodeName(ip,port,project_id, node_name):
    """
    This function will return a string that contains node_id according to project_id and node_name given in parameter

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project ID.
        param3 (str): GNS3 Node Name.

    Returns:
        str: String node id
    """
    
    nodesID = getGNS3NodesIDByProjectID(ip, port, project_id)
    
    for nodeID in nodesID:
        result = requests.get("http://" + ip + ":" + port +
                              "/v2/projects/" + project_id + "/nodes/" + nodeID)

        if json.loads(result.text)["name"] == node_name:
            return json.loads(result.text)["node_id"]

            
def getGNS3NodeNameByProjectIDAndNodeID(ip, port, project_id, node_id):
    """
    This function will return a string that contains node_name  according to project_id and node_id given in parameter

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project ID.
        param3 (str): GNS3 Node ID.

    Returns:
        str: String Hostname
    """
    result = requests.get("http://" + ip + ":" + port +
                          "/v2/projects/" + project_id + "/nodes/" + node_id)
    content = json.loads(result.text)
    exceptionGNS3Get200(result.status_code, content)


    return content["name"]


def getGNS3NodesByProjectName(ip, port, project_name):
    """
    This function will return a JSON that contains all information about the projects node according to project_name given in parameter

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project name.

    Returns:
        json: Json that contains all information about project nodes.
    """

    return getGNS3NodesByProjectName(ip, port, getGNS3ProjectIDByName(ip, port, project_name))


def getGNS3NodesByProjectID(ip, port, project_id):
    """
    This function will return a JSON that contains all information about the projects node according to project_id given in parameter

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project id.

    Returns:
        json: Json that contains all information about project nodes.
    """

    result = requests.get("http://" + ip + ":" + port +
                          "/v2/projects/" + project_id + "/nodes")
    content = json.loads(result.text)
    exceptionGNS3Get200(result.status_code, content)

    return content


def getGNS3NodesIDByProjectID(ip, port, project_id):
    """
    This function will return a LIST that contains all nodes_id in the according to project_name given in parameter

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project name.

    Returns:
        list: LIST that contains all nodes_id according to project_name.
    """
    
    result = requests.get("http://" + str(ip) + ":" + str(port) +
                          "/v2/projects/" + str(project_id) + "/nodes")
    
    content = json.loads(result.text)
    exceptionGNS3Get200(result.status_code, content)

    project_id_list = list()
    for node in content:
        project_id_list.append(node["node_id"])

    return project_id_list

def getGNS3NodesIDByProjectName(ip, port, project_name):
    """
    This function will return a LIST that contains all nodes_id in the project according to project_name given in parameter

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project name.

    Returns:
        list: LIST that contains all nodes_id according to project_name.
    """
    return getGNS3NodesIDByProjectID(ip, port, getGNS3ProjectIDByName(ip, port, project_name))

# -----------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------
# According to Project

def startGNS3NodesByProjectName(ip, port, project_name):
    """
    This function will start all nodes from project_name.

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project Name.

    """

    startGNS3NodesByProjectID(
        ip, port, getGNS3ProjectIDByName(ip, port, project_name))


def startGNS3NodesByProjectID(ip, port, project_id):
    """
    This function will start all nodes from project_id.

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project ID.

    """
    
    requests.post("http://" + ip + ":" + port + "/v2/projects/" + project_id + "/nodes/start", json={})


def stopGNS3NodesByProjectID(ip, port, project_name):
    """
    This function will start all nodes from project_name.

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project Name.

    """
    stopGNS3NodesByProjectID(
        ip, port, getGNS3ProjectIDByName(ip, port, project_name))


def stopGNS3NodesByProjectID(ip, port, project_id):
    """
    This function will start all nodes from project_id.

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project ID.

    """

    requests.post("http://" + ip + ":" + port + "/v2/projects/" +
                  project_id + "/nodes/stop", json={})


def getGNS3ProjectByName(ip, port, project_name):
    """
    This function will return a JSON that contains all information about the project_name given in parameter

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project name.

    Returns:
        json: Json that contains all information about project.
    """
    return getGNS3ProjectByID(ip, port,  getGNS3ProjectIDByName(ip, port, project_name))

def getGNS3ProjectByID(ip, port, project_id):
    """
    This function will return a JSON that contains all information about the project_id given in parameter

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project ID.

    Returns:
        json: Json that contains all information about project.
    """

    result = requests.get("http://" + ip + ":" + port + "/v2/projects/" + project_id)
    content = json.loads(result.text)
    exceptionGNS3Get200(result.status_code, content)

    return content


def getGNS3ProjectIDByName(ip, port, project_name):
    """
    This function will return a string that contains project_id according to the project_name given in parameter

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project name.

    Returns:
        string: string that contains project_id.
    """
    
    result = requests.get("http://" + ip + ":" + port + '/v2/projects')
    content = json.loads(result.text)
    exceptionGNS3Get200(result.status_code, content)

    project_id = ""
    for project in content:
        if project["filename"] in project_name:
            project_id = project["project_id"]
            break
    
    return project_id


def getGNS3ApplianceAvailables(ip, port):
    result = requests.get("http://" + ip + ":" + port + '/v2/appliances')
    content = json.loads(result.text)
    exceptionGNS3Get200(result.status_code, content)

    appliancesAvailable = list()    
    for app in content:
        appliancesAvailable.append(app["name"])
    
    return appliancesAvailable


# -----------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------
# According to GNS3 VM
def getGNS3Version(ip, port):
    """
    This function will return a string that contains GNS3 VM Version

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.

    Returns:
        string: string that contains GNS3 VM Version.
    """
    result = requests.get("http://" + ip + ":" + port + '/v2/version')

    content = json.loads(result.text)
    exceptionGNS3Get200(result.status_code, content)

    return content["version"]

# -----------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------
# According to Exception

def exceptionGNS3Get200(status_code, error_msg):
    """
    This function will raise an Exception if request status_code is not 200

    Args:
        param1 (str): HTTP request status_code.
    """
    if status_code is 403:
        raise Exception("Error "+str(status_code) +
                        " in HTTP GET  \n The project is not opened \n " + error_msg["message"])
    elif status_code is 405:
        raise Exception("Error "+str(status_code) +
                        " in HTTP GET  \n The Method Not Allowed \n " + error_msg["message"])
    elif status_code is not 200:
        raise Exception("Error "+str(status_code) +
                        " in HTTP GET \n " + error_msg["message"])


def exceptionGNS3Post204(status_code, error_msg):
    """
    This function will raise an Exception if request status_code is not 204

    Args:
        param1 (str): HTTP request status_code.
    """

    if status_code is not 204:
        raise Exception("Error in HTTP POST \n " + error_msg["message"])



# -----------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------
    

if __name__ == "__main__":
    main()
