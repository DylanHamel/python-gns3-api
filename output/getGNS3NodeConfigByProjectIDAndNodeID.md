Execute the code

```Python
print(getGNS3NodeConfigByProjectIDAndNodeID(
            ip, port, "4a0686cf-597b-4cc2-b811-bfed024cc03d",
            "b2d9f8a3-0fdc-4008-8e06-e1ef19da3841"))
```

Execute via shell

```bash
./gns3-api.py > getGNS3NodeConfigByProjectIDAndNodeID.txt
```

Result

```bash
(master*) » cat getGNS3NodeConfigByProjectIDAndNodeID.txt


!
!
!

!
! Last configuration change at 19:55:01 UTC Sun Feb 24 2019
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
!
hostname Router01
!
boot-start-marker
boot-end-marker
!
!
!
no aaa new-model
no ip icmp rate-limit unreachable
ip cef
!
!
!
!
!
!
no ip domain lookup
ip multicast-routing
no ipv6 cef
!
!
multilink bundle-name authenticated
!
!
!
!
!
!
!
!
!
ip tcp synwait-time 5
!
!
!
!
!
!
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 192.168.1.253 255.255.255.0
 ip pim sparse-mode
 standby 1 ip 192.168.1.1
 standby 1 priority 110
 standby 1 preempt
 standby 1 authentication ha-pass
 ip igmp version 3
 speed auto
 duplex auto
!
interface FastEthernet0/1
 no ip address
 shutdown
 speed auto
 duplex auto
!
interface FastEthernet1/0
 no ip address
 shutdown
 speed auto
 duplex auto
!
interface FastEthernet1/1
 no ip address
 shutdown
 speed auto
 duplex auto
!
ip forward-protocol nd
!
!
no ip http server
no ip http secure-server
!
!
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
 stopbits 1
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
 stopbits 1
line vty 0 4
 login
!
!
end
